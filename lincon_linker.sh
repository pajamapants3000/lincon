#!/bin/bash
#
# File   : lincon_linker.sh
# Purpose: Applies Linux configuration in this repository
# Author : Tommy Lincoln <pajamapants3000@gmail.com>
# License: MIT -- See LICENSE
# Notes  : See README for usage and behavior
# Updated: 02/11/2016
#

# TODO:
#       * use command line arguments to override id and type
#       * add -i interactive command to ask user before making each change
#       * quiet and verbose options
#       * logfile; debugging mode
#

                #*** Establish important constants ***#
# Location of this script, same as root of repository
scriptdir=$(dirname $(readlink -f $0))
# Name of subfolder containing configuration
conf_files_dir=${scriptdir}/conf_fs
# Name of subfolder for storing original files (append num days till delete)
save_files_dir=${scriptdir}/save
# Subfolder with lincon configuration
linc_conf_dir=${scriptdir}/linconf
#
# lid: comp identification; LINCON_ID if present, else ${CHRISTENED}-${SURNAME}
#+    +default to a unique tempX if none of these are available.
echo "checking for LINCON_ID"
if [[ -n ${LINCON_ID} ]]; then
    echo "setting lid to ${LINCON_ID}"
    lid=${LINCON_ID}
elif [[ -n ${CHRISTENED} && -n ${SURNAME} ]]; then
    echo "LINCON_ID not found"
    echo "checking for CHRISTENED, SURNAME..."
    echo "setting lid to ${CHRISTENED}-${SURNAME}"
    lid="${CHRISTENED}-${SURNAME}"
else    # no ID to use: create unique temp id
    echo "LINCON_ID not found"
    echo "CHRISTENED, SURNAME not found"
    i=1
    while [ -d ${save_files_dir}/temp${i} ]; do
        ((i++))
    done
    echo "setting lid to temp${i}"
    lid="temp${i}"
    i=
fi
# ltype: configuration "flavor"; LINCON_TYPE if present, else empty and unused
[ "${LINCON_TYPE}" ] && ltype=${LINCON_TYPE} || ltype=${lid}
#^^^    ^^^^    ^^^^    ^^^^    ^^^^    ^^^^    ^^^^    ^^^^    ^^^^    ^^^^

                #*** Important variables set; Let's begin! ***#
pushd $scriptdir    # work from root of repository
# prep backup folder; store `uname -a` output
mkdir -pv ${save_files_dir}/${lid}
uname -a > ${save_files_dir}/${lid}/INFO
#** The real work **#
for file in $(pushd ${conf_files_dir} && find . ); do
# Special case: all user files are in ${conf_files_dir}/home/, not e.g.
#+${conf_files_dir}/home/user/; this frees us from any dependence on
#+username or home directory location.
    if [[ $(echo ${file:1} | cut -d/ -f1-2) == '/home' ]]; then
        sysfile=${HOME}/$(echo ${file} | cut --complement -d/ -f1-2)
    else
        sysfile=${file:1}       # :1 eliminates "." first char in filenames
    fi
    if [ -f ${sysfile} -o -L ${sysfile} ]; then     # file or symlink
        if [[ $(echo ${file:1} | cut -d/ -f1-2) == '/home' ||
                $(id -u) -eq 0 ]]; then # only do home files unless run as root
# XXX: the following section presumes filenames don't end with "-" or "-tempX"
            if [ -f ${conf_files_dir}/${sysfile:1}-${lid} ]; then
                append="-${lid}"    # system-specific takes priority
            elif [ -f ${conf_files_dir}/${file}-${ltype} ]; then
                append="-${ltype}"  # check for configuration "flavor" next
            else
                append=             # take default
            fi
            if [[ -L ${sysfile} &&  # skip if already linked
                    $(readlink -f ${sysfile}) == \
                    ${conf_files_dir}/${file:2}${append} ]]; then
                echo "${sysfile} already linked"
                continue    # no change needs to be made
            fi  # already linked
# Now let's do it!
            mkdir -pv ${save_files_dir}/${lid}/$(dirname ${sysfile:1})
            echo "copying ..."
            cp -dv --preserve=all ${sysfile} ${save_files_dir}/${lid}/${sysfile:1}
            echo "linking ..."
            ln -sfv ${conf_files_dir}/${file:2}${append} ${sysfile}
        fi  # is user file or we are running as root
    fi  # file exists
done
pushd
echo "All original files have been moved to ${save_files_dir}/${lid}"
echo "Each has been successfully replaced by a link to a file in"
echo "  ${conf_files_dir}"
echo "use lid ${lid}"

