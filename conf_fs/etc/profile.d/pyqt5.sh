#
# Begin /etc/profile.d/pyqt5.sh

PYQT5_SIP_DIR=/usr/share/sip/PyQt5
export PYQT5_SIP_DIR

pathappend /opt/pyqt5/bin 	PATH
pathappend /opt/pyqt5 		PYTHONPATH

# End /etc/profile.d/pyqt5.sh


