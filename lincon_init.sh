#!/usr/bin/env bash
#
# File   : lincon_init.sh
# Purpose: initializes the linux configuration for lincon
# Author : Tommy Lincoln <pajamapants3000@gmail.com>
# License: MIT; See LICENSE
# Notes  : After cloning lincon, run this to get started.
# Created: 03/01/2016
# Updated: 03/02/2016
#

                        #***    Begin    ***#

