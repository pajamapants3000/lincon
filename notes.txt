lincon
*******
Linux Configuration - Notes

This thing needs some work! It's still hard to keep track of files and avoid
totally wrecking your config!

To start with, let's get rid of the whole system-wide config messing. We
can come back to that, perhaps as a special user. We already are kinda doing
that but... I think we should get the regular user working right before
messing as root.

First of all, we need an initial backup creation - we should keep a list of
files (or, as we've been doing, a directory of files to compare against) that
will be backed up and then linked ( not just linked! (or... ?)).

An auto-diff would be great. Before the initial setup, the program shows
the user what files will be changed and how, if the user chooses to go with
a preexisting configuration.

I think the ID and Type system is still good. Most computers should fit one
of a small set of types, with IDs applied on the few files that are more
particular.

If ID and Type aren't set, perhaps the initial setup can just copy all files,
giving them a unique suffix, and then linking them. Later these can be
renamed or relinked but in the meantime you still have a working config.


# Outline - init:{{{
# This should output a message welcoming the user and describing what this
# program does. It should check the environment for LINCON_ID and LINCON_TYPEs;
#+then prompt for them.
# Then scan for all files in lincon and whatever is found is moved to lincon
# with LINCON_ID appended and then linked. Prompt user with help on how to
# further customize. Allow user to list additional files to add, then exit.
#++ Can also add any number of LINCON_TYPE, to match first found for each
#+++file. For each, create empty file called LINCON_TYPE_XX-type; create
#+++file for LINCON_ID called LINCON_ID-id. This is how the program knows!
# Once each file is linked, the script then goes through any LINCON_TYPEs
#+added and prompts the user for them, including diffs.
#+All configuration-related files go in conf subfolder.
}}}
# Outline - share - with LINCON_TYPE or main{{{
# Ask for LINCON_TYPE or "main" (the untagged version)
# Ask for files to apply to
# FOR EACH FILE:
# Ask if the shared version should be -imported or ^exported (better
#+terminology?)
# -If imported: present with diff and confirm - offer to save diff for rerunning
#+later
# -If confirmed: backup current config, then link to main/type config (ALWAYS
#+link to own config, but now that one in turn links to the shared one)
#^If exported: present with diff and warning that this will affect a list of
#+systems (list them by name).
#^If confirmed: Produce warnings for the affected systems; backup the file
#^with a copy under each of their names. When they update, they should be
#+given the option to accept the new file, revert, or steal it back!
#+(but that's a note for lincon_update)
#+You're basically applying the 'import' option on all of the OTHER systems
#+linked to that type.
# NOTE: can offer diff for each option for user to compare before choosing, or
#+save diff to come back to later (so long as files are not changed in
#+the meantime)
# If LINCON_TYPE (or 'main') doesn't exist for that file, just create it and
#+link to it.
}}}
# Outline - unshare{{{
# Ask user if they want to just split off or recover latest backup; offier diff
}}}
# Outline - add{{{
# Add file(s)
}}}
# Outline - remove{{{
# remove file(s)
}}}
# Outline - unlink_all{{{
# Copy all files back to their normal places and get rid of all links
}}}
# Outline - update{{{
# pull and sync with repository. Inform user of all changes, including diffs
# backups go in "trash" (?) subfolder; a file in this folder tells how long
#+backups are kept - save_30 says save for 30 days; if multiples exist,
#+keep biggest and delete others; allow type/id-specific save times?
}}}

Ideas:
    * On update, scans user folder for all dotfiles and keeps record of which
        ones are modified. The idea here is to recommend dotfiles that are
        highly customized to be included.
    * Allow for aliases on filenames; e.g. bashrc, bash.bashrc. Also,
        /home/tommy/.config is a common location for $XDG_CONFIG_HOME, but the
        program should check the user's $XDG_CONFIG_HOME and use that instead
        if it exists and is different.
    * Create diffs with other IDs and TYPEs, to see if it might be a good
        idea to initiate a new sharing. Maybe create an algorithm that searches
        for good matches.
    * Use version control to manage history of changes; diffs
    * Permit backups/saves of configurations you like but aren't
        using at present
    * Curses interface? Other graphical interface? Not with shell script
        (or ...? what about that dialog program?)
    * create a folder for each file in conf_fs; maybe prefix with "_" or
        something to distinguish it; For files like bashrc and bash.bashrc,
        can create a symlink bash.bashrc -> bashrc; each file in the folder
        is named with the ID/TYPE and then what to call the file on the
        normal fs - full path name? maybe sep with "-" instead of "/".


linpro:
Also create a package manager called linpro (Linux Programs). Can have a
listing directory with each known program in the root as a folder, and in
each folder is a list of all dependencies of that program as a symlink to
that program in the root (Brilliant!). The symlinks can have some indicator
of whether it's a required, recommended, or optional dependency.



