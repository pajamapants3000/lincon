[Appearance]
ColorScheme=WhiteOnBlack
Font=Source Code Pro,13,-1,5,50,0,0,0,0,0

[General]
Command=vim -u ${HOME}/.vimrc.ycm
Directory=$CODEPATH
Environment=TERM=xterm-256color
Icon=/home/tommy/Copy/Pictures/Icons/gvim.png
LocalTabTitleFormat=vim - %D
Name=ViM-YouCompleteMe
Parent=FALLBACK/
StartInCurrentSessionDir=true

[Interaction Options]
UnderlineLinksEnabled=false

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=0
ScrollBarPosition=2
