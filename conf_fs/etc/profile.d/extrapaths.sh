#
# Begin /etc/profile.d/extrapaths.sh

if [ -d /usr/local/lib/pkgconfig ] ; then
        pathappend /usr/local/lib/pkgconfig PKG_CONFIG_PATH
fi
if [ -d /usr/local/bin ]; then
        pathappend /usr/local/bin
fi
if [ -d /usr/local/sbin -a $EUID -eq 0 ]; then
        pathappend /usr/local/sbin
fi

# go
[ -d ${GO_PREFIX} ] && pathappend ${GOROOT}/bin || (exit 0)

# plan9port
[ -d ${PLAN9} ] && pathappend  ${PLAN9}/bin || (exit 0)

# plan9 - suckless' slimmer plan9port
[ -d ${PLAN9} ] && pathappend  ${PLAN9}/bin || (exit 0)

