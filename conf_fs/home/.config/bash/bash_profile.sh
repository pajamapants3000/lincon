#
# Begin ~/.bash_profile
# User dependent .bash_profile file; executed by bash(1) for login shells.
#
# DEPENDENCIES:
#   /etc/profile.d/bash_function.sh - pathprepend function

# Ensure the system profile is sourced (for some reason it isn't sometimes)
source /etc/profile

# Add users local prefix bin
if [ -d "${HOME}/.local/bin" ] ; then
  pathprepend   ${HOME}/.local/bin     PATH
fi

# Set PATH so it includes user's private bin if it exists
if [ -d "${HOME}/bin" ] ; then
  pathprepend   ${HOME}/bin     PATH
fi

###
## Additional, specific-type sources
###
# source the user's bash_envar.sh if it exists
if [ -f "${XDG_CONFIG_HOME}/bash/bash_envar.sh" ] ; then
  source "${XDG_CONFIG_HOME}/bash/bash_envar.sh"
elif [ -f "${HOME}/.config/bash/bash_envar.sh" ] ; then
  source "${HOME}/.config/bash/bash_envar.sh"
fi

# source the user's bash_function.sh if it exists
if [ -f "${XDG_CONFIG_HOME}/bash/bash_function.sh" ] ; then
  source "${XDG_CONFIG_HOME}/bash/bash_function.sh"
elif [ -f "${HOME}/.config/bash/bash_function.sh" ] ; then
  source "${HOME}/.config/bash/bash_function.sh"
fi

# source the user's extrapaths.sh if it exists
if [ -f "${XDG_CONFIG_HOME}/bash/extrapaths.sh" ] ; then
  source "${XDG_CONFIG_HOME}/bash/extrapaths.sh"
elif [ -f "${HOME}/.config/bash/extrapaths.sh" ] ; then
  source "${HOME}/.config/bash/extrapaths.sh"
fi

# Set MANPATH so it includes users' private man if it exists
if [ -d "${XDG_DATA_HOME}/man" ]; then
  pathprepend   ${XDG_DATA_HOME}/man    MANPATH
elif [ -d "${HOME}/.local/share/man" ]; then
  pathprepend   ${HOME}/.local/share/man    MANPATH
fi

# Set INFOPATH so it includes users' private info if it exists
if [ -d "${XDG_DATA_HOME}/info" ]; then
  pathprepend   ${XDG_DATA_HOME}/info   INFOPATH
elif [ -d "${HOME}/.local/share/info" ]; then
  pathprepend   ${HOME}/.local/share/info   INFOPATH
fi

source ${HOME}/.bashrc

# End ~/.bash_profile
