#
# Begin /etc/profile.d/bash_completion_additions.sh
#
# System-wide completion options
#
# These completion tuning parameters change the default behavior of bash_completion:
#
# Define to access remotely checked-out files over passwordless ssh for CVS
# COMP_CVS_REMOTE=1
#
# Define to avoid stripping description in --option=description of './configure --help'
COMP_CONFIGURE_HINTS=1
#
# Define to avoid flattening internal contents of tar files
COMP_TAR_INTERNAL_PATHS=1
#
#if [ -d /etc/bash_completion.d ]; then
#    for file in $(ls -A /etc/bash_completion.d); do
#        if [ -r /etc/bash_completion.d/${file} ]; then
#            source /etc/bash_completion.d/${file}
#        fi
#    done
#fi
#
# End /etc/profile.d/bash_completion_additions.sh
