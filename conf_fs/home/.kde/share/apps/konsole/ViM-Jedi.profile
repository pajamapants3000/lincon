[Appearance]
ColorScheme=WhiteOnBlack
Font=Source Code Pro,13,-1,5,50,0,0,0,0,0

[General]
Command=vim -u ${HOME}/.vimrc.jedi
Directory=$CODEPATH
Environment=TERM=xterm-256color
Icon=/home/tommy/Copy/Pictures/Icons/ViM-Jedi.xpm
LocalTabTitleFormat=vim - %D
Name=ViM-Jedi
Parent=FALLBACK/
StartInCurrentSessionDir=true

[Interaction Options]
UnderlineLinksEnabled=false

[Scrolling]
HistoryMode=0
ScrollBarPosition=2
