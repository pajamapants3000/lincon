[Appearance]
BoldIntense=true
ColorScheme=weechat
Font=Anonymous Pro,12,-1,5,50,0,0,0,0,0

[General]
Command=/bin/bash -c weechat
Directory=${HOME}
Name=weechat
Parent=FALLBACK/

[Scrolling]
HistoryMode=0
ScrollBarPosition=2
