#
# Begin /etc/profile.d/pyqt4.sh

PYQT4_SIP_DIR=/usr/share/sip/PyQt4
export PYQT4_SIP_DIR

pathappend /opt/pyqt4/bin 	PATH
pathappend /opt/pyqt4 		PYTHONPATH

# End /etc/profile.d/pyqt4.sh

