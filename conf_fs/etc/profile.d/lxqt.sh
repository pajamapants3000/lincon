#
# Begin /etc/profile.d/lxqt.sh
#

LXQTDIR=/opt/lxqt

pathappend /opt/lxqt/bin           PATH
pathappend /opt/lxqt/share/man/    MANPATH
pathappend /opt/lxqt/lib/pkgconfig PKG_CONFIG_PATH
pathappend /opt/lxqt/share         XDG_DATA_DIRS

export LXQTDIR
export PATH MANPATH PKG_CONFIG_PATH XDG_DATA_DIRS

# End /etc/profile.d/lxqt.sh

