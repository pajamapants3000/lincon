" File ~/.vimrc -- Jedi -- Lilu-LFS Flavor
"******************************************
" This file sources the main vimrc, specialized for Python
"+by using Jedi completion instead of YouCompleteMe. It
"+subsequently applies configuration specific to Lilu-LFS
"******************************************

" Source the main vimrc.jedi
source $DRIVEPATH/Systems/Universal/vimfiles/python.vimrc

" Apply special configurations for Lilu-LFS

