# The following lines were added by compinstall

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' format 'Completion: %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent pwd .. directory
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' menu select=5
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %B%p%b -- %SScrolling active: current selection at %l%s
zstyle :compinstall filename '/home/tommy/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory extendedglob
unsetopt nomatch
bindkey -v
# End of lines configured by zsh-newuser-install

setopt prompt_subst

# Git info in prompt
#********************
GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
# Takes space-separated list of [verbose, name, legacy, git, svn] or "auto"
GIT_PS1_SHOWUPSTREAM="auto"
GIT_PS1_STATESEPARATOR=$SP
# Takes [contains, branch, describe, or default]
GIT_PS1_DESCRIBE_STYLE="default"
GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_HIDE_IF_PWD_IGNORED=1
source ${HOME}/bin/git-prompt.sh

# Terminal Prompt - Work in Progress for Zsh
#****************
#source ${HOME}/.config/bash/PS1escapes
#preGIT="${Cyan}\u@\h ${BIPurple}[${Color_Off} \W ${BIPurple}]${BRed}"
#postGIT="${BIPurple}\%${Color_Off} "
#PS1=$preGIT'$(__git_ps1 " (%s)")'$postGIT
# Basic prompt
#PS1="$GREEN\u [ $NORMAL\w$GREEN ]\$ $NORMAL"
#
# No color, for Zsh until I can get it to work!
preGIT="%n@%m [ %d ]"
postGIT="%% "
# NO IDEA why this doesn't work! The prompt is ok but no git info.
# It must not reevaluate anything that isn't a '%' escape.
PS1=${preGIT}\$(__git_ps1)${postGIT}
