# Begin /etc/profile.d/xdg.sh

# xdg directories assignments
XDG_CONFIG_DIRS=/etc/xdg
XDG_CONFIG_HOME=$HOME/.config
XDG_DATA_DIRS=/usr/local/share:/usr/share
XDG_DATA_HOME=$HOME/.local/share

export XDG_CONFIG_DIRS XDG_CONFIG_HOME XDG_DATA_DIRS XDG_DATA_HOME

# End /etc/profile.d/xdg.sh
