" File ~/.vimrc
"******************************************
" This file sources the main vimrc, passing
" config_type to tell it what settings to
" use. Defaults to standard C/C++ settings
" if not specified otherwise.
" Settings may be specified on the command
" line when invoking vim:
" $ vim --cmd "let config_type='python'"
" Available config_type values:
" c, python, lua, mutt, min, none, all
"******************************************
let config_default = 'c'
let vimfiles = $HOME.'/repo/vimfiles'

" If a value was passed make sure it is usable!
if !exists("config_type") ||
  \!filereadable(vimfiles . '/config_' . config_type . '.vimrc')
    let config_type = config_default   " otherwise use the default
endif
" Source the main vimrc
exe 'source ' . vimfiles . '/vimrc'

" Apply special configurations for Lilu-LFS

