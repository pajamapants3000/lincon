#
# mutt configuration for pajamapants3000@gmail.com
#
#   currently not used - imap, smtp external

# Receive Options - Built-in IMAP
#set imap_user=pajamapants3000@gmail.com
#set imap_pass=$my_pw_gmail-pajamapants3000
#set folder = imaps://pajamapants3000@imap.gmail.com/

# Receive Options - External IMAP
set folder = ${HOME}/Mail/pajamapants3000@gmail.com

# Receive Options - All
set spoolfile = +Inbox
set postponed = +Drafts
set record = +Sent
#mailboxes +inbox


# Send Options - Built-in SMTP
#set smtp_url=smtps://pajamapants3000:$my_pw_gmail-pajamapants3000@smtp.gmail.com
#set envelope_from_address=pajamapants3000@gmail.com
# If we don't want to manually store outgoing mail, either
#   becausse it's already being done
#unset record

# Connection Options
#set ssl_force_tls = yes

# Send Options - External SMTP
set sendmail="/usr/bin/msmtp"

# Send Options - All
set realname="Tommy Lincoln"
set from=pajamapants3000@gmail.com
set signature=$my_cfgdir/signature.pajamapants3000.gmail.com

# Hook
account-hook $folder "set imap_user=pajamapants3000@gmail.com imap_pass=$my_pw_gmail-pajamapants3000"

# vi:ft=muttrc
