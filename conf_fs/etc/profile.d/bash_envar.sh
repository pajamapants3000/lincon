#
# /etc/profile.d/bash_envar.sh
# All environment vars that should always be made available

# Window Manager paths
KDE_PREFIX=/opt/kde
KF5_PREFIX=/opt/kf5
LXQT_PREFIX=/opt/lxqt
LXQTDIR=/opt/lxqt
ENL_PREFIX=/opt/enlightenment
export KDE_PREFIX KF5_PREFIX LXQT_PREFIX LXQTDIR ENL_PREFIX

# Qt-related paths
QT4DIR=/opt/qt4
QT5DIR=/opt/qt5
PYQT4_SIP_DIR=/usr/share/sip/PyQt4
PYQT5_SIP_DIR=/usr/share/sip/PyQt5
export QT4DIR QT5DIR PYQT4_SIP_DIR PYQT5_SIP_DIR

# Additional program paths
GOROOT=/opt/go
PLAN9=/usr/local/plan9port
P9=/usr/local/plan9
export GO_PREFIX PLAN9 P9

# Useful path definitions
DRIVEPATH=/media/Sync/google_drive-otripleg
CODEPATH=${DRIVEPATH}/Code
PYPATH=${CODEPATH}/Python
CPATH=${CODEPATH}/C
CXXPATH=${CODEPATH}/C++
JPATH=${CODEPATH}/Java
BASHPATH=${CODEPATH}/Bash
TCLPATH=${CODEPATH}/Tcl
LUAPATH=${CODEPATH}/Lua
export DRIVEPATH CODEPATH PYPATH CPATH CXXPATH JPATH BASHPATH TCLPATH

VISUAL=vim
EDITOR=vim
export VISUAL EDITOR

