#!/bin/sh
#
# wallpaper-feh.sh
# Displays a background image wallpaper using feh

# Aang in space
#IMAGE=${1:-'/home/tommy/images/backgrounds/avatar_aang_the_last_airbender_by_nobruh-d6q2uvq.jpg'}
# Four circle elements; black filled with multiple colors
#IMAGE=${1:-'/home/tommy/images/backgrounds/294430.jpg'}
# Large moon; grey and black
#IMAGE=${1:-'/home/tommy/images/backgrounds/1440x900_hd_wallpaper_110_zixpkcom.10212725_large.jpg'}
# Teardrop; simple thin color surrounded by a pretty grey/black smudge/shadow
IMAGE=${1:-'/home/tommy/images/backgrounds/avatarTLB-teardrop_elements.jpg'}

feh  --bg-scale ${IMAGE}

# End wallpaper-feh.sh

