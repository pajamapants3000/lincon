# Begin bash_prompt.sh
# Set the prompt seen in an interactive bash shell

# Shell Prompt - PS1
#^^^^^^^^^^^^^^^^^^^^
# Provides default prompt for non-login shells, specifically shells started
#+in the X environment. Can be overridden by user bashrc.
# Root has red prompt, non-root users will have a green prompt.
NORMAL="\[\e[0m\]"
RED="\[\e[1;31m\]"
GREEN="\[\e[1;32m\]"
if [[ $EUID == 0 ]] ; then
  PS1="$RED\u [ $NORMAL\w$RED ]# $NORMAL"
else
  PS1="$GREEN\u [ $NORMAL\w$GREEN ]\$ $NORMAL"
fi

unset script RED GREEN NORMAL

# End bash_prompt.sh

