#!/bin/bash
#

QTDIR=/opt/qt5

pathappend /opt/qt5/bin			    PATH
pathappend /opt/qt5/lib/pkgconfig 	PKG_CONFIG_PATH

export PATH PKG_CONFIG_PATH

# Begin /etc/profile.d/qt5.sh
# ** Qt5 changes for KF5
#

QT_PLUGIN_PATH=/usr/lib/qt5/plugins
pathappend /opt/qt5/plugins             QT_PLUGIN_PATH
pathappend /opt/qt5/qml                 QML_IMPORT_PATH
pathappend /opt/qt5/qml                 QML2_IMPORT_PATH

export QT_PLUGIN_PATH QML_IMPORT_PATH QML2_IMPORT_PATH

# This addition is actually to make up for poor planning on
#+my qtcreator installation
pathappend /opt/qt5/share		XDG_DATA_DIRS

export XDG_DATA_DIRS

# End /etc/profile.d/qt5.sh

