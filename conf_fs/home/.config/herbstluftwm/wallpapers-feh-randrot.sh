#!/bin/bash
#
# wallpaper.sh
# Uses feh to generate a new background wallpaper every few minutes

# Kill existing wallpaper process
source ${TEMP}/pid_wallpapers-feh-randrot
[ ${PID_WALLPAPERS_FEH_RANDROT} ] && kill ${PID_WALLPAPERS_FEH_RANDROT} || (exit 0)
# Store PID for THIS instance of this panel; put in temporary file since we
#+can't put it in the environment without sourcing this and it's caller!
echo "export PID_WALLPAPERS_FEH_RANDROT=$$" > ${TEMP}/pid_wallpapers-feh-randrot

# Set refresh time in minutes
REFRESH_TIME=1

shopt -s nullglob
cd ~/.config/backgrounds

while true; do
    files=()
    for i in *.jpg *.png; do
        [[ -f $i ]] && files+=("$i")
    done
    range=${#files[@]}

    ((range)) && feh --bg-scale "${files[RANDOM % range]}"

    sleep ${REFRESH_TIME}m
done
