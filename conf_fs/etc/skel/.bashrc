#
# ~/.bashrc
#

# Set interactive behavior: aliases, functions, etc.

# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

# Start by sourcing a global config if it exists. This will be
#+overridden by this personal config, which is why the global is
#+sourced first.
if [ -f /etc/bashrc ]; then
      . /etc/bashrc
elif [ -f /etc/bash.bashrc ]; then
      . /etc/bash.bashrc
fi

# Shell Options
#
# see manpage under "The Shopt Builtin" (section 4.3.2)
#
# -s (set) enables, -u (unset) disables
#
# When changing directory small typos can be ignored by bash
#+for example, cd /vr/lgo/apaache would find /var/log/apache
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
# Make bash append rather than overwrite the history on disk
shopt -s histappend
shopt -s hostcomplete
# Use case-insensitive filename globbing
shopt -s nocaseglob
# Don't use ^D to exit
# set -o ignoreeof
# Don't wait for job termination notification
# set -o notify

# Completion options
#
# Source bash_completion
if [ -f /etc/bash_completion ]; then
	    . /etc/bash_completion
fi
#
xhost +local:root > /dev/null 2>&1
#
# I believe this enables completion for commands following sudo
complete -cf sudo
#
# These completion tuning parameters change the default behavior of bash_completion:
#
# Define to access remotely checked-out files over passwordless ssh for CVS
# COMP_CVS_REMOTE=1
#
# Define to avoid stripping description in --option=description of './configure --help'
# COMP_CONFIGURE_HINTS=1
#
# Define to avoid flattening internal contents of tar files
# COMP_TAR_INTERNAL_PATHS=1
#
# Uncomment to turn on programmable completion enhancements.
# Any completions you add in ~/.bash_completion are sourced last.
# [[ -f /etc/bash_completion ]] && . /etc/bash_completion

# History Options
#
# Don't put duplicate lines in the history.
#export HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
export HISTCONTROL=ignoreboth
#
# Ignore some controlling instructions
# HISTIGNORE is a colon-delimited list of patterns which should be excluded.
# The '&' is a special pattern which suppresses duplicate entries.
# export HISTIGNORE=$'[ \t]*:&:[fb]g:exit'
# export HISTIGNORE=$'[ \t]*:&:[fb]g:exit:ls' # Ignore the ls command as well
#
# Whenever displaying the prompt, write the previous line to disk
# export PROMPT_COMMAND="history -a"
export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}

# Umask
#
# /etc/profile sets 022, removing write perms to group + others.
# Set a more restrictive umask: i.e. no exec perms for others:
# umask 027
# Paranoid: neither group nor others have any perms:
# umask 077

# Aliases
#
# Some people use a different file for aliases
if [ -f "${HOME}/.bash_aliases" ]; then
  source "${HOME}/.bash_aliases"
fi
#
# Some shortcuts for different directory listings
#alias ls='ls -hF --color=tty'                 # classify files in colour
alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -l --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -lA --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias dir='ls --color=auto --format=vertical'
alias vdir='ls --color=auto --format=long'
# alias l='ls -CF'                              #
alias cls='clear'
# Interactive operation...
alias rm='rm -i'
alias cp="cp -i"                          # confirm before overwriting something
alias mv="mv -i"                          # confirm before overwriting something
# Default to human readable figures
alias df='df -h'                          # human-readable sizes
alias du='du -h'
alias free='free -m'                      # show sizes in MB
# Misc :)
# alias less='less -r'                          # raw control characters
# alias whence='type -a'                        # where, of a sort
alias grep='grep --color'                     # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour
alias sgrep='grep --color=tty -d skip'
alias rgrep='grep --color=tty -d recurse'
alias vp='vim PKGBUILD'
alias vs='vim SPLITBUILD'

# Functions
#
# Some people use a different file for functions
if [ -f "${HOME}/.bash_functions" ]; then
  source "${HOME}/.bash_functions"
fi
#
# ex - archive extractor
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
# My own creations #
#*******************
# rgr - grep -R [string] * | less
# Does a grep search for [string] in all files from current
#+directory, recursively, and outputs to less
rgr ()
{
    SEARCHSTRING=$1
    grep -R "${SEARCHSTRING}" * | less
}
# rgri - grep -iR [string] * | less
# Same as rgr, but case-insensitive
rgri ()
{
    SEARCHSTRING=$1
    grep -iR "${SEARCHSTRING}" * | less
}
export -f rgr rgri

# vim stuff
export EDITOR=vim
export VISUAL=vim
alias vi=vim

#  vi keybindings! Set with corresponding lines in ~/.inputrc
#+ Make sure you know this is set if it is active! Can be very
#+ confusing and frustrating otherwise.
#set -o vi

# End ~/.bashrc

