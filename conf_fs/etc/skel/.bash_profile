#
# ~/.bash_profile
#

# Set PATH so it includes user's private bin if it exists
if [ -d "${HOME}/bin" ] ; then
  PATH="${HOME}/bin:${PATH}"
fi

# Set user XDG config and data directories
if [ -d "${HOME}/.config" ] ; then
    export XDG_CONFIG_HOME=${HOME}/.config
fi

if [ -d "${HOME}/.local/share" ] ; then
    export XDG_DATA_HOME=${HOME}/.local/share
fi

# Set MANPATH so it includes users' private man if it exists
# if [ -d "${HOME}/man" ]; then
#   MANPATH="${HOME}/man:${MANPATH}"
# fi

# Set INFOPATH so it includes users' private info if it exists
# if [ -d "${HOME}/info" ]; then
#   INFOPATH="${HOME}/info:${INFOPATH}"
# fi

# End ~/.bash_profile

