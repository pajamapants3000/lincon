#
# Begin ~/.bashrc
# User dependent .bashrc file; executed by bash(1) for interactive shells.

# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

# Make sure system settings are sourced
[ -r /etc/bashrc ] && source /etc/bashrc

# source the user's bash_alias if it exists
if [ -f "${HOME}/.config/bash/bash_alias.sh" ] ; then
  source "${HOME}/.config/bash/bash_alias.sh"
fi

# source the user's bash_hist if it exists
# WARNING: do NOT source .bash_history!!!
if [ -f "${HOME}/.config/bash/bash_hist.sh" ] ; then
  source "${HOME}/.config/bash/bash_hist.sh"
fi

# Shell Options
#
# See man bash for more options...
#
# Don't wait for job termination notification
# set -o notify
#
# Don't use ^D to exit
# set -o ignoreeof
#
# Use case-insensitive filename globbing
# shopt -s nocaseglob
#
# Make bash append rather than overwrite the history on disk
# shopt -s histappend
#
# When changing directory small typos can be ignored by bash
# for example, cd /vr/lgo/apaache would find /var/log/apache
# shopt -s cdspell

# source the user's bash_completion if it exists
if [ -f "${HOME}/.config/bash/bash_completion.sh" ] ; then
  source "${HOME}/.config/bash/bash_completion.sh"
fi

# Umask
#
# /etc/profile sets 022, removing write perms to group + others.
# Set a more restrictive umask: i.e. no exec perms for others:
# umask 027
# Paranoid: neither group nor others have any perms:
# umask 077

# Git info in prompt
#********************
GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
# Takes space-separated list of [verbose, name, legacy, git, svn] or "auto"
GIT_PS1_SHOWUPSTREAM="auto"
GIT_PS1_STATESEPARATOR=$SP
# Takes [contains, branch, describe, or default]
GIT_PS1_DESCRIBE_STYLE="default"
GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_HIDE_IF_PWD_IGNORED=1
source ${HOME}/bin/git-prompt.sh

# Terminal Prompt
#****************
source ${HOME}/.config/bash/PS1escapes.sh
preGIT="${Cyan}\u@\h ${BIPurple}[${Color_Off} \W ${BIPurple}]${BRed}"
postGIT="${BIPurple}\$${Color_Off} "
PS1=$preGIT'$(__git_ps1 " (%s)")'$postGIT
# Basic prompt
#PS1="$GREEN\u [ $NORMAL\w$GREEN ]\$ $NORMAL"
#

# gpg-agent
export GPG_TTY=$(tty)

# End ~/.bashrc
