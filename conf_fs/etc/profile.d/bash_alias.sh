#!/bin/bash
#
# Begin /etc/profile.d/bash_alias.sh
# Sets the system-wide alias environment variables

alias ls='ls --color=auto'
alias grep='grep --color=auto'

# For Qt
alias setqt4='source setqt4'
alias setqt5='source setqt5'
alias loadqt4='source loadqt4'
alias loadqt5='source loadqt5'

# For WMs
alias setkf5='source setkf5'
alias setkde4='source setkde4'
alias setenl='source setenl'
alias setlxqt='source setlxqt'


# End /etc/profile.d/bash_alias.sh
