#!/bin/bash

# ${XDG_CONFIG_HOME}/bash/bash_envar
# User defined environment variables
#
# Extra paths
SYNCPATH=/media/Sync
DRIVEPATH=${HOME}/Gdrive-otripleg
CODEPATH=${DRIVEPATH}/Code
CPATH=${CODEPATH}/C
CXXPATH=${CODEPATH}/C++
PYPATH=${CODEPATH}/Python
JPATH=${CODEPATH}/Java
BASHPATH=${CODEPATH}/Bash
LUAPATH=${CODEPATH}/Lua
TCLPATH=${CODEPATH}/TclTk
LISPPATH=${CODEPATH}/Lisp
GOPATH=${CODEPATH}/Go
RUSTPATH=${CODEPATH}/Rust
export SYNCPATH DRIVEPATH CODEPATH CPATH CXXPATH JPATH
export PYPATH LUAPATH TCLPATH LISPPATH GOPATH RUSTPATH

# Project directories for further convenience
VAS=/home/tommy/repo/bitbucket-pajamapants3000-hg/virtual_agent_setup
export VAS

# Temp folder; dot to keep it hidden, since it isn't usually needed to be seen
#+Assigned to both common definitions.
TEMP=${HOME}/.temp
TMP=${HOME}/.temp
export TEMP TMP

PGDATA=/home/tommy/.config/pgsql/data

# Ctags can alternatively be set in ~/.ctags; recursive option is
#+ --recurse=[yes|no]; left out so it is to be specified each time.
CTAGS='--tag-relative=yes --fields=+l --languages=C,C++,Lua,Perl,Python,Ruby,Scheme,Awk,Sh,Tcl,Make,Go,Lisp'

# Repository sites
PJ3K_BB='https://bitbucket.org/pajamapants3000'
PJ3K_GH='https://github.com/pajamapants3000'
export PJ3K_BB PJ3K_GH

# Pager settings, including convenience function
MANPAGER="vim -c MANPAGER -"
export MANPAGER
page () {
    $1 | $MANPAGER
}
export -f page

# For lua-5.1
eval "LUA_PATH=/usr/share/lua/5.1/?.lua\;/usr/share/lua/5.1/?/init.lua\;/usr/lib/lua/5.1/?.lua\;/usr/lib/lua/5.1/?/init.lua\;./?.lua\;./?/init.lua\;${LUAPATH}/5.1/?.lua\;${LUAPATH}/5.1/?/init.lua"
eval "LUA_CPATH=/usr/lib/lua/5.1/?.so\;/usr/lib/lua/5.1/loadall.so\;./?.so\;${LUAPATH}/5.1/?.so\;${LUAPATH}/5.1/loadall.so"
export LUA_PATH LUA_CPATH

# For lua-5.2 and lua-5.3 - allows versioned variables (thank you!)
for i in $(seq 2 3); do
    eval "LUA_PATH_5_${i}=/usr/share/lua/5.${i}/?.lua\;/usr/share/lua/5.${i}/?/init.lua\;/usr/lib/lua/5.${i}/?.lua\;/usr/lib/lua/5.${i}/?/init.lua\;./?.lua\;./?/init.lua\;${LUAPATH}/5.${i}/?.lua\;${LUAPATH}/5.${i}/?/init.lua"
    eval "LUA_CPATH_5_${i}=/usr/lib/lua/5.${i}/?.so\;/usr/lib/lua/5.${i}/loadall.so\;./?.so\;${LUAPATH}/5.${i}/?.so\;${LUAPATH}/5.${i}/loadall.so"
    export LUA_PATH_5_${i} LUA_CPATH_5_${i}
done

# Include user-specific library locations
pathappend ${HOME}/.local/lib LD_LIBRARY_PATH

# Include user-specific header locations
pathappend ${CPATH}                 CPATHS
pathappend ${HOME}/.local/include   CPATHS

# vifm environment
VIFM=${HOME}/.config/vifm
VIFMRC=${VIFM}/vifmrc

# rustup
pathappend ${HOME}/.cargo/bin
RUST_SRC_PATH=${HOME}/repo/rust/src
CARGO_HOME=${HOME}/.cargo
export RUST_SRC_PATH CARGO_HOME

