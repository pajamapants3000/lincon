#!/usr/bin/env bash
#
# File   : lincon_include.sh
# Purpose: contains shared functions and variables for lincon scripts
# Author : Tommy Lincoln <pajamapants3000@gmail.com>
# License: MIT; See LICENSE
# Notes  : source this script when writing other lincon scripts
# Created: 03/02/2016
# Updated: 03/02/2016
#

                #*** Establish important constants ***#
# Location of this script, bin subfolder of repository
scriptdir=$(dirname $(readlink -f $0))
# Location of linconf configuration
linconf_dir="${scriptdir}/../linconf"
# Location of stored Linux configuration files
storage_dir="${scriptdir}/../storage"
# Location of saved backups
backup_dir="${scriptdir}/../backup"
# Location of main online lincon repository
lincon_repo='https://www.bitbucket.org/pajamapants3000/linux_config'


                    #***    Define functions    ***#
welcome_user ()     #{{{
{
    echo 'Welcome to lincon - the ultra-cool Linux configuration manager!'
    echo '(...press enter)' && read
    echo 'This script will guide you through the setup process; once we are'
    echo 'finished you will have your Linux configuration safely stored in'
    echo 'a folder that you can share between computers and use next time'
    echo 'you need to recover your settings.'
    echo '(...press enter)' && read
    echo "LET'S BEGIN!"
    sleep 1
    clear
}
#}}}
obtain_lid ()   #{{{
{
    echo 'One of the ways lincon keeps track of multiple systems and'
    echo 'their different configurations is by assigning an identification'
    echo 'to each computer.'
    sleep 2
    if [[ -n $(find ${linconf_dir} -name MY_LINCON_ID_*) ]]; then
        lid=$(find ${linconf_dir} -name MY_LINCON_ID_* | \
            sed 's/MY_LINCON_ID_//')
        echo 'Looks like you already have a name assigned to this computer...'
        sleep 1
        echo "Keep using ${lid}?"
        read proceed_lid
        if [[ ${proceed_lid} == "yes" || ${proceed_lid} == "y" ]]; then
            return 0
        fi
    fi
    prompt_for_lid()
}
#}}}
prompt_for_lid ()   #{{{
{
    echo 'What name would you like to give this computer?'
    echo -n 'Name: '
    local lid
    read lid
    while check_availability(lid); do
        if [[ -z ${lid} ]]; then
            'How about we at least TRY to think up a name... ?'
        else
            echo "It looks like ${lid} is taken!"
            echo 'Do you want to use it anyway? Whatever computer that has'
            echo "the name ${lid} will have the same configuration."
            read proceed_lid_dup
            if [[ ${proceed_lid_dup} == "yes" ||
                  ${proceed_lid_dup} == "y"   ]]; then
                break
            fi
        fi
        sleep 1
        clear
        echo 'What name would you like to give this computer?'
        echo -n 'Name: '
        read lid
    done
    echo "${lid} it is then!"
    touch ${linconf_dir}/MY_LINCON_ID_${lid}
    uname -a ${linconf_dir}/LINCON_ID_${lid}
}
#}}}
check_availability ()   #{{{
{
    local lid = $1
    if [[ -z $lid ]]; then return 1; fi
    return (ls ${linconf_dir}/LINCON_ID_* | sed 's/.*LINCON_ID_//' | \
        grep "\<${lid}\>" > dev/null)
}
#}}}
obtain_ltypes()     #{{{
{
    echo 'An unshared configuration file will always be stored with this'
    echo "computer's LINCON_ID appended to the filename. However, you"
    echo 'may also opt to share a configuration file with other, similar'
    echo 'systems. To do so, instead of appending a specific LINCON_ID,'
    echo 'we use a LINCON_TYPE, a label that can be shared among multiple'
    echo 'systems. We may always set a custom LINCON_TYPE when we decide'
    echo 'to share a particular file; but to simplify the process, we may'
    echo 'wish to associate a specific LINCON_TYPE with this computer. This'
    echo 'will be used by default when you ask to start sharing a config file.'
    echo '(...press enter)' && read
    echo "There is a special LINCON_TYPE which we call 'main'. When you ask"
    echo "to share with the 'main' LINCON_TYPE, you will be using the version"
    echo 'of the file with nothing appended to the filename. Thus, once we'
    echo 'set a LINCON_TYPE, in addition to our LINCON_ID, we have three'
    echo 'levels of sharing possible - that is: 1) not shared, 2) shared'
    echo 'with a select group, and 3) shared unselectively with the'
    echo "'main' group."
    echo '(...press enter)' && read
    prompt_for_ltype()
}
#}}}
prompt_for_ltype()  #{{{
{
    echo 'The following LINCON_TYPEs have been created:'
    echo $(find ${linconf_dir} -name LINCON_TYPE_${ltype} | \
        sed 's/LINCON_TYPE//')
    echo "What is this computer's LINCON_TYPE?"
    echo '(you may leave blank, use an existing one, or create a new one)'
    echo -n 'LINCON_TYPE: '
    read ltype
    if [[ -n ${ltype} && ! -f ${linconf_dir}/MY_LINCON_TYPE_${ltype} ]]; then
        touch ${linconf_dir}/MY_LINCON_TYPE_${ltype}
    fi
}
#}}}
home_scan_and_move ()    #{{{
{
    for file in $(pushd ${storage_dir}/home && find . ); do
        home_link_new_file(${file})
    done
            
}
#}}}
home_link_new_file ()   #{{{
{
    file=$1
    local lid
    get_lid() || noid()
    sysfile=${HOME}/$(echo ${file} | cut --complement -d/ -f1-2)
    if [[ -f ${sysfile} ]]; then
        echo "Copying file ${sysfile} ..."
        cp -v ${sysfile} "${file}_LINCON_ID_${lid}"
        echo "Creating symlink ${sysfile}"
        rm -f ${sysfile}
        ln -sv "${file}_LINCON_ID_${lid}" ${sysfile}
    fi
}
#}}}
share_with_group_import ()     #{{{
{
    file=$1     # refers to file on user's system/home folder, NOT in .lincon
    actual_file=$(readlink -e ${file})  # this is WHATEVER has the content
    actual_file_root=${actual_file%_LINCON_*}
    my_file=${actual_file_root}_LINCON_ID_${lid}
    local lid, ltype
    get_lid() || noid()
    get_ltype()
    ltype="${2:-$ltype}"
    if ! [[ -f ${actual_file_root}_LINCON_TYPE_${ltype} ]]; then
        echo "This group doesn't have a shared version of this file!"
        echo "We will set your current version to be the one shared with"
        echo "the other computers in group ${ltype}"
        sleep 2
        share_with_group_export(${file}, ${lid}, ${ltype})
    else
        echo "Creating backup ${my_file}-$(date +%Y%m%d)"
        cp -v ${actual_file} \
            ${backup_dir}/${my_file}-$(date +%Y%m%d)
        echo "Linking to shared version ${actual_file_root}_LINCON_${ltype}"
        rm -f ${my_file}
        ln -sv ${actual_file_root}_LINCON_TYPE_${ltype} ${my_file}
    fi
}
#}}}
share_with_main_import ()   #{{{
{
    file=$1     # refers to file on user's system/home folder, NOT in .lincon
    actual_file=$(readlink -e ${file})
    actual_file_root=${actual_file%_LINCON_*}
    local lid
    get_lid() || noid()
    my_file=${actual_file_root}_LINCON_ID_${lid}
    backup_file="${backup_dir}/${actual_file_root##*/}"
    backup_file="${backup_file}_LINCON_ID_${lid}-$(date +%Y%m%d)"
    echo "Creating backup ${backup_file}"
    cp ${actual_file} ${backup_file}
    echo "Linking to shared version ${actual_file_root}"
    rm -f ${my_file}
    ln -sv ${actual_file_root} ${my_file}
}
#}}}
share_with_group_export ()     #{{{
{
    file=$1     # refers to file on user's system/home folder, NOT in .lincon
    actual_file=$(readlink -e ${file})
    actual_file_root=${actual_file%_LINCON_*}
    local lid, ltype
    get_lid() || noid()
    get_ltype() 
    # other important filenames & paths
    my_file=${actual_file_root}_LINCON_ID_${lid}
    group_file=${actual_file_root}_LINCON_TYPE_${ltype}
    #
    if [[ -f ${group_file} ]]; then
        for f in $(find ${storage_dir} \
                -name "${actual_file_root##*/}_LINCON_ID_"); do
            if [[ "$(readlink $f)" == "${group_file}" ]]; then
                cp "$f" "${backup_dir}/${f##*/}-$(date +%Y%m%d)"
            fi
        done
    fi
    echo "Setting your config as the ${actual_file_root##*/} for ${ltype}"
    rm -f ${group_file}
    cp ${actual_file} ${group_file}
    rm -f ${my_file}
    ln -sv ${group_file} ${my_file}
}
#}}}
share_with_main_export ()   #{{{
{
    file=$1     # refers to file on user's system/home folder, NOT in .lincon
    actual_file=$(readlink -e ${file})
    actual_file_root=${actual_file%_LINCON_*}
    local lid, ltype
    get_lid() || noid()
    get_ltype()
    # other important filenames & paths
    my_file=${actual_file_root}_LINCON_ID_${lid}
    group_file=${actual_file_root}
    #
    if [[ -f ${group_file} ]]; then
        for f in $(find ${storage_dir} \
                -name "${actual_file_root##*/}_LINCON_ID_"); do
            if [[ "$(readlink $f)" == "${group_file}" ]]; then
                cp "$f" "${backup_dir}/${f##*/}-$(date +%Y%m%d)"
            fi
        done
    fi
    echo "Setting your config as the main ${actual_file_root##*/}"
    rm -f ${group_file}
    cp ${actual_file} ${group_file}
    rm -f ${my_file}
    ln -sv ${group_file} ${my_file}
}
#}}}
# noid: run this if a LINCON_ID cannot be found{{{
noid ()
{
    echo "error: no LINCON_ID found. Please run lincon_init."
    exit 1
}
#}}}
get_lid ()  #{{{
{
    str_id='MY_LINCON_ID_'
    lid=$(find ${linconf_dir} -name ${str_id}* | cut -b $((${#str_id}+1))-)
    if [[ -z ${lid} ]]; then
        return 1
    fi
    return 0
}
#}}}
get_ltype ()    #{{{
{
    str_type='MY_LINCON_TYPE_'
    ltype=$(find ${linconf_dir} -name ${str_type}* \
            | cut -b $((${#str_type}+1))-)
}
#}}}

